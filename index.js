function Trainer(name, age, friends, pokemon){
	this.trainerName = name;
	this.trainerAge = age;
	this.trainerFriends = friends;
	this.trainerPokemon = pokemon;
}

let myTrainer = new Trainer ("Ash Ketchum", 10, ["Brock", "Misty"], ["Venusaur", "Charizard", "Blastoise"])
console.log(myTrainer);


/*let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Venusaur", "Charizard", "Blastoise"],
	introduce: function(){
		console.log("Hello my name is: " + this.name + ". Age: " + this.age)
	}

}

trainer.introduce()
console.log("Here is my pokemons!")
console.log(trainer.pokemon)*/

console.log(myTrainer.trainerPokemon[1] + "! I choose you! ");

function Pokemon (name, level){
	this.pokemonName = name;
	this.pokemonLevel = level;
	this.pokemonHealth = 2 * level;
	this.pokemonAttack = level;

	this.tackle = function(targetPokemon){
		console.log(this.pokemonName + " tackles " + targetPokemon.pokemonName)
		console.log(targetPokemon.pokemonName + " is now reduced to " + (targetPokemon.pokemonHealth - this.pokemonAttack));

			targetPokemon.pokemonHealth -= this.pokemonAttack;

			if(targetPokemon.pokemonHealth <= 0){
				console.log(targetPokemon.pokemonName + " fainted !");

			}
			else{
				console.log("Attack again!")
			}
		} 
	}
let charizard = new Pokemon (myTrainer.trainerPokemon[1], 70);
let harden = new Pokemon ("Harden", 70);

console.log(charizard)
console.log(harden)
charizard.tackle(harden)
charizard.tackle(harden)








